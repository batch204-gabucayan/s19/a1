// alert("YOU'RE IN MASTER!");


// Start of login
let username, password, roleInit, roleFin;

function userLogin() {
	username = prompt("Please input your username: "); // User will input a username
	password = prompt("Input your password: "); // user will intput password here
	roleInit = prompt("Your role please: "); // user will input their role
	if(roleInit != null) {
		roleFin = roleInit.toLowerCase();
	}
}

userLogin();

// if() statement that will show if user did not complete login information
if (username==''||password==''||roleFin==''||username==null||password==null) {
	alert("Please input all needed information!");
} else { // if all information included, initiate switch() case
 	switch(roleFin){
 		case 'admin':
 			alert("Welcome back to the class portal, admin!");
 			break;

 		case 'teacher':
 			alert("Thank you for logging in, teacher!");
 			break;

 		case 'student':
 			alert("Welcome to the class portal, student!");
 			break;

 		default: // alert if role entered does not match above choices
 			alert("Role out of range.");
 			break;
 	}
 }

function aveCal(gradeOne, gradeTwo, gradeThree, gradeFour) {
	gradeOne = parseInt(prompt("First Grade : "));
	gradeTwo = parseInt(prompt("Second Grade : "));
	gradeThree = parseInt(prompt("Third Grade: "));
	gradeFour = parseInt(prompt("Fourth Grade: "));
 	let totalGrade = gradeOne+gradeTwo+gradeThree+gradeFour;
 	let avgGrade = Math.round(totalGrade/4);

 	// Print Average Grade and their corresponding Letter

 	 if(avgGrade<=74) {
 	 	console.log("Hello, student, your average is "+avgGrade+". The letter equivalent is F");
 	 } else if(avgGrade>=75&&avgGrade<=79) {
 	 	console.log("Hello, student, your average is "+avgGrade+". The letter equivalent is D");
 	 } else if(avgGrade>=80&&avgGrade<=84) {
 	 	console.log("Hello, student, your average is "+avgGrade+". The letter equivalent is C");
 	 } else if(avgGrade>=85&&avgGrade<=89) {
 	 	console.log("Hello, student, your average is "+avgGrade+". The letter equivalent is B");
 	 } else if (avgGrade>=90&&avgGrade<=95) {
 	 	console.log("Hello, student, your average is "+avgGrade+". The letter equivalent is A");
 	 } else if(avgGrade>=96) {
 	 	console.log("Hello, student, your average is "+avgGrade+". The letter equivalent is A+");
 	 } else {
 	 	console.log("Please enter valid parameters!"); // Print this if parameters given are invalid
 	 }
 	console.log(`Please type "aveCal()" function in the console to compute another Average.`);

 	return;
 }

 aveCal(); // declaring function
 // console.log(avgGrade);

